#!/bin/sh

abort() {
	echo "$@" >&2
	exit 1
}

check_exec() {
	"$@" >/dev/null 2>&1
}

check_ruby() {
	check_exec ruby --version
}

check_docker() {
	check_exec docker --version
}

if check_ruby; then
	HAS_RUBY=true
else
	HAS_RUBY=false
fi

if ! check_docker; then
	abort "Não encontrou o comando 'docker', abortando"
fi

if [ ! -f registros.db ]; then
	abort "Não encontrou o arquivo 'registros.db', necessário para fazer o cadastro dos runners"
fi

run_ruby() {
	if $HAS_RUBY; then
		ruby "$@"
	else
		docker run --rm -i -v `pwd`:/app -w /app ruby:3.0 ruby "$@"
	fi
}

CREATE_REG_SCRIPT=true
if [ -f registros.sh ]; then
	read -p "Deseja sobrescrever o registros.sh previamente existente? [sN] " CONFIRM
	if [ "$CONFIRM" != 'S' -a "$CONFIRM" != 's' ]; then
		CREATE_REG_SCRIPT=false
	fi
fi

if $CREATE_REG_SCRIPT; then
	cat > registros.sh << EOF
#!/bin/sh
set -e
EOF
	run_ruby drivers/ruby/gera_runner_script.rb < registros.db >> registros.sh || abort "Não foi possível gerar o script de registro com sucesso, abortando"
fi

cat registros.sh

read -p "Deseja executar o script acima para registrar os runners? [sN] " CONFIRM

if [ "$CONFIRM" = 'S' -o "$CONFIRM" = 's' ]; then
	sh registros.sh
fi
