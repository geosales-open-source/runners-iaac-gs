# runners-iaac-gs

Repositório destinado a cadastro de runners do gitlab na GeoSales

# Formato

O formato proposto é:

```csv
<versão>:<id>:<url-gitlab>:<token>:<imagem>:<tags>:<privilégios>
```

Por exemplo:

```csv
1:jeff-mipha-dind:https\://gitlab.com:abcjas:docker\:19.03.12-dind:dind:1
1:jeff-mipha-geral:https\://gitlab.com:abcjas:openjdk\:15-jdk::0
1:jeff-mipha-teste::abcjas:openjdk\:15-jdk:untagged:0
1:jeff-mipha-rj:https\://gitlab.com:abcjas:openjdk\:15-jdk:java,ruby:
1:jeff-mipha-rju::abcjas:openjdk\:15-jdk:java,untagged,ruby:0
```

Essa configuração gera 4 registros de runners distintos:

- `jeff-mipha-dind`, com a tag `dind` e privilégios, com o token `abcjas` usando como alvo o `https://gitlab.com`, imagem padrão `docker:19.03.12-dind`
- `jeff-mipha-geral`, sem tag e sem privilégios, com o token `abcjas` usando como alvo o `https://gitlab.com`, imagem padrão `openjdk:15-jdk`
- `jeff-mipha-test`, sem tag e sem privilégios, com o token `abcjas` usando como alvo o `https://gitlab.com`, imagem padrão `openjdk:15-jdk`
- `jeff-mipha-rj`, com as tags `java` e `ruby` e sem privilégios, com o token `abcjas` usando como alvo o `https://gitlab.com`, imagem padrão `openjdk:15-jdk`
- `jeff-mipha-rju`, com as `java` e `ruby`, rodando jobs sem tags e sem privilégios, com o token `abcjas` usando como alvo o `https://gitlab.com`, imagem padrão `openjdk:15-jdk`

## Os campos

Os campos aceitam caracteres imprimíveis. Algumas sequências de escape são aceitas, como `\\` sendo interpretado
como `\` e `\:`, sendo interpretado como `:`.

### Versão

O campo de versão é um numeral referente ao formato da linha onde estão essas informações. No caso, a versão
a qual nos referimos aqui no momento é a versão `1`.

Deve ser um inteiro.

### Id

Identificador do runner. Será usado como nome do runner a ser cadastrado.

Pode receber caracteres escapados, se aceito pelo GitLab os caracteres arbitrários como identificador 
do runner.

### URL GitLab

URL de onde está hospedado o GitLab.

Vazio significa `https://gitlab.com`

Aceita uma URL. Caso precise colocar esquema específico ou a porta, use a sequência de escape.

### Token

O token para registro do runner no GitLab.

Pode receber caracteres escapados, caso o token contenha um caracter que deva ser escapado.

### Imagem

Imagem docker padrão informada para o runner. Campo obrigatório. Tag da imagem, definida pelo caracter separador `:`, precisa naturalmente de escape.

Vide os exemplos, a imagem do Docker-in-docker aparece como `docker\:19.03.12-dind`, sendo portanto interpretada como `docker:19.03.12-dind`.

### Tags

As tags que o runner pode pegar, separadas por vírgulas.

Pode ter uma tag com caracteres escapados, se o GitLab aceitar o caracter na tag.

Vazio significa que roda jobs sem tag. Se for fornecido uma tag e desejar que o runner rode jobs sem tag,
existe a "pseudo-tag" `untagged`.

### Privilégios

Se o runner roda com privilégio.

Espera-se `0` para sem privilégio ou `1` para com privilégio.

Vazio significa sem privilégio.

## Futuro compatibilidade

A versão fornecida como primeiro campo serve para futuro compatibilidade do sistema. Atualmente, apesar
de apenas aceitar a versão 1 atualmente, pode eventualmente mudar quais as colunas esperadas no CSV.
