require_relative "gera_runner_script"

puts "favor, verificar se está fazendo sentido as coisas"
puts "para cada linha do CSV de exemplo do README, vai ser impresso:"
puts "- a própria linha"
puts "- a linha do objeto remontado como CSV"
puts "- a linha de comando para rodar o comando docker para subir o runner"
puts
puts "note que a linha e a linha remontada podem ter representações distintas"
puts "por exemplo, na linha de id 'jeff-mipha-test', que não se faz necessário explicitar o 'untagged', a remontagem não incluirá essa pseudo-tag"
puts
puts

'1:jeff-mipha-dind:https\://gitlab.com:abcjas:docker\:19.03.12-dind:dind:1
1:jeff-mipha-geral:https\://gitlab.com:abcjas:openjdk\:15-jdk::0
1:jeff-mipha-teste::abcjas:openjdk\:15-jdk:untagged:0
1:jeff-mipha-rj:https\://gitlab.com:abcjas:openjdk\:15-jdk:java,ruby:
1:jeff-mipha-rju::abcjas:openjdk\:15-jdk:java,untagged,ruby:0'.each_line do |line|
  if line[-1] == "\n"
    line = line[0, line.size - 1]
  end
  rinfo = RunnerInfo.CreateFromLine(line)
  puts "##{line}"
  puts "##{rinfo.to_dsv}"
  puts rinfo.docker_cli(gitlab_runner_local_dir: "~/runner-info", tty: false)
  puts
end
