class RunnerInfo
  def self.CreateFromLine(linhaConf)
    values = {}
    tokenize(linhaConf) do |i, c|
      values [case i
      when 0 then :vid
      when 1 then :id
      when 2 then :url
      when 3 then :token
      when 4 then :imagem
      when 5 then :tags
      when 6 then :privilegios
      end] = c
    end
    RunnerInfo.create values
  end

  def self.tokenize(linha, &bloc)
    idx = 0
    el = 0
    jump = false
    for i in (0..linha.length) do
      if jump
        jump = false
        next
      end
      if linha[i] == "\\"
        jump = true
        next
      end
      if linha[i] == ":"
        yield el, linha[idx, i - idx].gsub(/\\(.)/, "\\1")
        el += 1
        idx = i + 1
      end
    end
    if idx < linha.length
      if linha[-1] == "\n"
        sub = linha[-2] == "\r" ? 2 : 1
      else
        sub = 0
      end
      yield el, linha[idx, linha.length - idx - sub].gsub(/\\(.)/, "\\1")
    end
  end

  def self.create(kw)
    RunnerInfo.new vid:kw[:vid], id:kw[:id], url:kw[:url], token:kw[:token], imagem:kw[:imagem], tags:kw[:tags], privilegios:kw[:privilegios]
  end

  def initialize(vid:, id:, url:, token:, imagem:, tags:, privilegios:)
    raise "versão incompatível" unless vid == "1"
    raise "precisa informar um id" if id.empty?
    raise "precisa informar um token" if token.empty?
    raise "precisa informar uma imagem" if imagem.empty?

    @vid = vid
    @id = id
    @url = url.empty? ? "https://gitlab.com" : url
    @token = token
    @imagem = imagem

    @tags = []
    if tags.empty?
      @untagged = true
    else
      @untagged = false
      tags.split(",").each do |tag|
        if tag == "untagged"
          @untagged = true
        else
          @tags.push tag
        end
      end
    end
    @privilegios = privilegios == "1"
  end

  def tag_list
    @tags.join ","
  end

  def cli
    cli = []
    cli.push "gitlab-runner register"
    cli.push "--executor docker" # executor modo docker, showzeira
    cli.push "-n" # não interativo
    cli.push "--name '#{@id}'"
    cli.push "--url '#{@url}'"
    cli.push "--docker-image '#{@imagem}'"
    cli.push "--registration-token '#{@token}'"
    cli.push "--tag-list '#{tag_list}'" unless @tags.empty?
    cli.push "--run-untagged " if @untagged
    cli.push "--docker-privileged" if @privilegios

    cli.join " "
  end

  def docker_cli gitlab_runner_local_dir: '`pwd`/', docker_socket_base: "/var/run/docker.sock", tty: true
    "docker run --rm " + (tty ? "-i -t " : " ") +
        "-v #{gitlab_runner_local_dir}:/etc/gitlab-runner -v #{docker_socket_base}:/var/run/docker.sock " +
        "gitlab/" + cli
  end

  def metadado_tags
    return "" if @tags.empty?
    return tag_list unless @untagged
    tag_list + ",untagged"
  end

  def to_dsv
    info = []
    info.push @vid
    info.push @id
    info.push @url
    info.push @token
    info.push @imagem
    info.push metadado_tags
    info.push @privilegios ? "1" : "0"
    info.map { |w| w.gsub(/([\\:])/, '\\\\\1') }.join ":"
  end
end

if __FILE__ == $0
  ARGF.each_line do |linha_conf|
    puts RunnerInfo.CreateFromLine(linha_conf).docker_cli
  end
end
